import { getCartLocal, totalCount, displayLoading, hideLoading, renderProductList, addToCart, eventHandler, calPrice } from "./controller/controller.js"


let productList = [];
let cartList = getCartLocal();
window.cartList = cartList;
let BASE_URL = "https://62f8b754e0564480352bf3c3.mockapi.io";


document.getElementById("totalCount").innerHTML = `${totalCount(cartList)}`;

// call API
export let renderProductService = () => {
    displayLoading();
    console.log("Yes");
    axios({
        url:`${BASE_URL}/phone`,
        method: "GET",
    })
    .then(function(res){
        hideLoading();
        console.log(res.data);
        productList = res.data;
        window.productList = productList;
        renderProductList(productList);
        eventHandler(productList, cartList);

    })
    .catch(function(err){
        console.log(err);
    })
}
window.renderProductService = renderProductService;
// render function
renderProductService();



// render Cart UI
export let renderCartService = () => {
    console.log("renderCartList", cartList);
    let content = "";
    let contentHTML = "";
    let totalAmount = 0;
    for(let newItem of window.cartList){
        console.log("newItem",newItem);
        let newItemPrice = calPrice(newItem.product.price, newItem.quantity);
        console.log("newItemPrice",newItemPrice);
        content = /*html*/ `
        <table class="table">
        <tbody>
            <tr>
                <td><img src="${newItem.product.img}"></td>
                <td><p>${newItem.product.name}</p></td>
                <td class="d-flex p-3"><button type="button" class="btn btn-primary" onclick="window.increaseItem('${newItem.product.id}', window.cartList)">+</button>
                    <p id="${newItem.product.id}itemCount">${newItem.quantity}</p>
                    <button type="button" class="btn btn-primary" onclick="window.decreaseItem('${newItem.product.id}', window.cartList)">-</i></button></td>
                <td><p id="${newItem.product.id}itemPrice">${newItemPrice}</p></td>
                <td><button type="button" class="btn btn-outline-danger" onclick="window.removeItem('${newItem.product.id}', window.cartList)">Remove</button></td>
            </tr>
        </tbody>
        </table>
        `
        contentHTML += content;
        totalAmount += newItemPrice;
    }
    // console.log('contentHTML',contentHTML);
    document.getElementById("modalBody").innerHTML = `${contentHTML} Total Amount:$ <span id="totalAmount">${totalAmount}</span>` ;
   
}
document.querySelector("#cartRender").addEventListener('click', renderCartService);

// function clear item
export let clearCart = () => {
    window.cartList = [];
    renderCartService();
    document.getElementById("totalCount").innerHTML = 0;
}
window.clearCart = clearCart;


