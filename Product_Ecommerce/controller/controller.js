import { CartItem } from "../models/productClass.js";
import { renderCartService } from "../main.js";

let list = []
let cart = [];
let product = {};
let quantity = 0;
let index = 0 ; 


export let renderProductList = (list) => {
    let content = "";
    let contentHTML = "";
    list.forEach((item) => {
        content = /*html*/ 
    `<div class="col-3 my-3">
        <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="${item.img}" alt="Card image cap">
        <div class="card-body">
        <h5 class="card-title">${item.name}</h5>
        <p class="card-text">
          ${item.type}</br>
          ${item.desc} </br>
          Screen: ${item.screen} </br>
          Back Camera: ${item.backCamera} </br>
          Front Camera: ${item.frontCamera}
        </p>
        <div class="d-flex justify-content-between">
        <div class="badge badge-success" style="width:100px; padding:10px; margin-bottom: 10px">$${item.price}</div>
        <a href="#" id="btnSelect${item.id}" class="btn btn-primary">Chọn mua</a>
        </div>          
        </div>
    </div>
    </div>`;
        contentHTML += content;
    })
    document.getElementById("prdContainer").innerHTML = contentHTML;
}
window.renderProductList = renderProductList

// find Object Index
export let findObjectIndex = (id,list) => {
    index = list.findIndex((item) => item.id === id)
    console.log("findObjectIndex list",list);
    console.log("findObjectIndex id",id);
    console.log("findObjectIndex index",index);
    return index;
}

// add event listener
export let eventHandler = (list,cart) => {
    list.forEach((item) => {
        const addFnt = () => {
            addToCart(`${item.id}`,list, cart)
        }
        document.querySelector(`#btnSelect${item.id}`).addEventListener('click', addFnt);
    })
}


// function Add To Cart
export let addToCart = (id,list,cart) => {
    findObjectIndex(id,list); // index  
    let item = list[index];
    const newItem = new CartItem(product,quantity);
    newItem.product = item;
    console.log(newItem);

    // find item in cart
    let indexCart = 0;
    indexCart = cart.findIndex((item) => item.product.id === newItem.product.id);
    console.log("itemIndex", indexCart);

    if(indexCart < 0){
        newItem.quantity = 1; 
        cart.push(newItem);
        console.log("cartList",cart);
    } else { 
        cart[indexCart].quantity++; 
        console.log("cartList",cart);
    }
    document.getElementById("totalCount").innerHTML = `${totalCount(cart)}`;
    // save cart to local
    saveCartLocal(cart);
}



// calculate price
export let calPrice = (amount,quantity) => {
    return (amount*quantity);
}
window.calPrice = calPrice;


// function save cart to local storage
export let saveCartLocal = (cart) => {
    localStorage.setItem("cartLocalStorage", JSON.stringify(cart));
}


// function get cart local storage
export const getCartLocal = () => {
    let cartEmp = [];
    let data = JSON.parse(localStorage.getItem("cartLocalStorage"));
    console.log("JSON parse", data);
    if(data === undefined || data === null ){
        return [];
    } else {
        console.log(data);
        data.forEach((item) => {
            const cartItem = new CartItem( item.product, item.quantity );
            cartEmp.push(cartItem);   
        })   
        console.log("cartLocalStorage",cartEmp);
        return cartEmp;
    }   
}

// filter item
export let filterProduct = (type,list) => {
    let result = list.filter(item => {
        return item.type === type;        
    });
    console.log(result);
    renderProductList(result);
}
window.filterProduct = filterProduct;

// find item in cart
export let findItemInCart = (id,cart) => {
    index = cart.findIndex((item) => item.product.id === id)
    console.log("findObjectIndex list",cart);
    console.log("findObjectIndex id",id);
    console.log("findObjectIndex index",index);
    return index;
}

// function increase 
export let increaseItem = (id, cart) => {
    console.log("ABC",cart);
    findItemInCart(id, cart); //index
    console.log("cart",cart);
    cart[index].quantity++;
    let totalAmount = 0;
    cart.forEach((item) => {
        console.log("item",item);
        totalAmount += item.calItemPrice();
        console.log("totalAmount", totalAmount);
    })
    renderCartService();
    saveCartLocal(cart);
    document.getElementById("totalCount").innerHTML = `${totalCount(cart)}`;
}
window.increaseItem = increaseItem;


// function decrease item
export let decreaseItem = (id, cart) => {
    findItemInCart(id, cart); //index
    console.log("cart",cart);
    cart[index].quantity--;
    let totalAmount = 0;
    cart.forEach((item) => {
        totalAmount += item.calItemPrice();
        console.log("totalAmount", totalAmount);
    })
    renderCartService();
    saveCartLocal(cart);
    document.getElementById("totalCount").innerHTML = `${totalCount(cart)}`;
}
window.decreaseItem = decreaseItem;

// function remove item
export let removeItem = (id,list) => {
    findObjectIndex(id,list);
    list.splice(index,1);
    console.log(list);
    renderCartService();
    saveCartLocal(cart);
}
window.removeItem = removeItem;

// total count 
export let totalCount = (cart) => {
    let totalCount = 0;
    for(let item of cart) {
        totalCount += item.quantity;
    }
    console.log("totalCount", totalCount);
    return totalCount;
}


// display / hide loading
export let displayLoading = () => {
    document.getElementById("loading").style.display = "flex";
}
export let hideLoading = () => {
    document.getElementById("loading").style.display = "none";
}