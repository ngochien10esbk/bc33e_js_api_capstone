export let renderProductListV2 = (list) => {
  let contentHTML = "";
  list.forEach((products) => {
    let content = `
        <tr>
        <td>${products.id}</td>
        <td>${products.name}</td>
        <td>$${products.price}</td>
        <td><img width="80px"src="${products.img}"></td>
        <td>
        <p class="text-primary font-weight-bold">${products.desc}</p>
        <span>Screen: ${products.screen}</span>,
        <span>BackCamera: ${products.backCamera}</span>,
        <span>FrontCamera: ${products.frontCamera}</span>
        </td>
        <td >
        <div class="d-flex align-items-center">
        <button data-toggle="modal" data-target="#themSP" onclick="editProduct(${products.id})" type="button" class="btn bg-light text-warning" btn-lg btn-block"><i class="fa-solid fa-pen-to-square"></i></button>
        <button onclick="deleteProduct(${products.id})" type="button" name="" id="" class="btn bg-light text-danger" btn-lg btn-block"><i class="fa-solid fa-trash"></i></button>
        </div>
 
        </td>
        </tr>
        `;
    contentHTML += content;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

export let layThongTinTuFormList = () => {
  const id = document.getElementById("txt-id").value;
  const name = document.getElementById("txt-tenSP").value;
  const price = document.getElementById("txt-gia").value;
  const screen = document.getElementById("txt-screen").value;
  const backCamera = document.getElementById("txt-backCamera").value;
  const frontCamera = document.getElementById("txt-fontCamera").value;
  const img = document.getElementById("txt-hinhAnh").value;
  const desc = document.getElementById("txt-moTa").value;
  const type = document.getElementById("txt-loai").value;
  return {
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
  };
};

window.layThongTinTuFormList = layThongTinTuFormList;

export let showThongTinLenFormList = (data) => {

    document.getElementById("txt-id").value =data.id;
    document.getElementById("txt-tenSP").value =data.name;
    document.getElementById("txt-gia").value = data.price;
    document.getElementById("txt-screen").value = data.screen;
    document.getElementById("txt-backCamera").value = data.backCamera;
    document.getElementById("txt-fontCamera").value = data.frontCamera;
    document.getElementById("txt-hinhAnh").value = data.img;
    document.getElementById("txt-moTa").value = data.desc;
    document.getElementById("txt-loai").value = data.type;

};
window.showThongTinLenFormList = showThongTinLenFormList;

