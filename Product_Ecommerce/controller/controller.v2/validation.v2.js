export let validation = {
  kiemTraRong: (valueInput, txtError) => {
    let string = valueInput;
    let trimmed = string.trim();
    if (trimmed == "") {
      document.getElementById(txtError).style.display = "inline";
      document.getElementById(txtError).innerText = "Trường này khổng để trống!";
      return false;
    } else {
      document.getElementById(txtError).style.display = "none";
      return true;
    }
  },
  kiemTraDoDaiKyTu: (valueInput, txtError, min, max) => {
   
    
    let inputLength = valueInput.length;

    if (inputLength < min || inputLength > max) {
      document.getElementById(txtError).style.display = "inline";
      document.getElementById(
        txtError
      ).innerText = `Độ dài từ ${min} đến ${max} kí tự`;
      return false;
    } else {
      document.getElementById(txtError).style.display = "none";
      return true;
    }
  },
  kiemTraSo: (valueInput, txtError) => {
    
    let numbers = /^[0-9]+$/;
    if (valueInput.match(numbers)) {
      document.getElementById(txtError).style.display = "none";
      return true;
    } else {
      document.getElementById(txtError).style.display = "inline";
      document.getElementById(txtError).innerText = "Giá phải nhập ký tự số";
      return false;
    }
  },
  kiemTraKichThuocManHinh: (valueInput, txtError, min) => {
    let screen = valueInput*1;
    if (screen >= min) {
      document.getElementById(txtError).style.display = "none";
      return true;
    }
    else {
      document.getElementById(txtError).style.display = "inline";
      document.getElementById(txtError).innerText = `Kích thước màn hình phải lớn hơn ${min}`;
      return false;
    }

  }
};
